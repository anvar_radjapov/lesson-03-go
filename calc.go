package main

import (
	"fmt"
	"math"
	"os"
	"text/tabwriter"
)

func main() {
	var amount, period, rate float64
	var periodicity float64 = 12

	fmt.Print("Enter amount of credit : ")
	fmt.Scan(&amount)
	fmt.Print("Enter period in years : ")
	fmt.Scan(&period)
	fmt.Print("Enter the rate : ")
	fmt.Scan(&rate)
	rate = rate / 100

	payment(amount, periodicity, period, rate)
}

func payment(amount, periodicity, period, rate float64) {
	var pay float64
	var totalPayment float64 = 0.0
	var IA, PA, Bal float64 = 0.0, 0.0, 0.0

	pay = (1 - (math.Pow(1+rate/periodicity, -periodicity*period))) / (rate / periodicity)
	totalPayment = amount / pay
	Bal = amount
	month := 1

	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 8, 8, 0, '\t', 0)
	defer w.Flush()

	fmt.Fprintf(w, "\n %s\t %s\t %s\t%s\t%s\t", "M", "Payment", "Interest", "Principal", "Balance")
	fmt.Fprintf(w, "\n %s\t %s\t %s\t%s\t%s\t", "----", "----", "----", "----", "----")
	for month <= int(period)*int(periodicity) {
		IA = Bal * rate / periodicity
		PA = totalPayment - IA
		Bal = Bal - PA
		fmt.Fprintf(w, "\n %v\t%.2f\t%.2f\t%.2f\t%.2f\t\n", month, totalPayment, PA, IA, Bal)

		month++
	}
}
